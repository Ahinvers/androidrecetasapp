package com.example.recipes;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private EditText et_nombr;
    private TextView txt_ingred,txt_cal,textonombrereceta;
    private Button bt_buscar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.txt_ingred = (TextView) findViewById(R.id.txt_ingre);
        this.textonombrereceta = (TextView) findViewById(R.id.textonombrereceta);
        this.txt_cal = (TextView) findViewById(R.id.txt_cal);
        this.et_nombr = (EditText) findViewById(R.id.et_nombre);
        this.bt_buscar = (Button) findViewById(R.id.bt_buscar);


        bt_buscar.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                final String nombre = et_nombr.getText().toString().trim();


                String url = "https://api.edamam.com/search?q="+nombre+"&app_id=a2cba4bc&app_key=2961fde6380f5070ed6b618001bdcf1a";
                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject respuestaJSON = new JSONObject(response);

//pruebagitlab2

                                    JSONArray hitsJSON = respuestaJSON.getJSONArray("hits");
                                    JSONObject primer = hitsJSON.getJSONObject(0);
                                    JSONObject recipesJSON  = primer.getJSONObject("recipe");
                                    String NombreRe = recipesJSON.getString("label");
                                    textonombrereceta.setText(NombreRe);
                                    JSONArray in = recipesJSON.getJSONArray("ingredientLines");
                                    String ingre = "" ;

                                    for(int i = 0; i<in.length();i++){

                                        ingre = ingre + in.getString(i)+ "\n" ;


                                    }
                                    txt_ingred.setText(ingre);


                                    JSONObject nutJSON = recipesJSON.getJSONObject("totalNutrients");
                                    JSONObject enerJSON =nutJSON.getJSONObject("ENERC_KCAL");
                                    String enerlabel = enerJSON.getString("label");
                                    String enerq = enerJSON.getString("quantity");
                                    String eneru = enerJSON.getString("unit");

                                    JSONObject fatJSON =nutJSON.getJSONObject("FAT");
                                    String fatlabel =fatJSON.getString("label");
                                    String fatq = fatJSON.getString("quantity");
                                    String fatu = fatJSON.getString("unit");

                                    JSONObject fasatJSON =nutJSON.getJSONObject("FASAT");
                                    String fasatlabel = fasatJSON.getString("label");
                                    String fasatq = fasatJSON.getString("quantity");
                                    String fasatu = fasatJSON.getString("unit");


                                    String todo = enerlabel + " = " + enerq + " - " + eneru + "\n" +
                                            fatlabel + " = " + fatq + " - " + fatu + "\n" +
                                            fasatlabel + " = " + fasatq + " - " + fasatu + "\n" ;

                                    txt_cal.setText(todo);






                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);

            }
        });

    }



}
